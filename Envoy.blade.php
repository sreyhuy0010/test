@setup
require __DIR__.'/vendor/autoload.php';
(new \Dotenv\Dotenv(__DIR__, '.env'))->load();

$server = env("DEPLOY_SERVER", 'deploy@root.linode');
$deployBranch = env("DEPLOY_BRANCH", 'master');
$gitServer = env("GIT_SERVER","git@gitlab.com:NHEL");
$repository = env("REPO_NAME","phnompenhbid");
$baseDir = env("BASE_DIR","/home/deploy");
$releasesDir = env("RELEASES_DIR","{$baseDir}/releases");
$currentDir = env("CURRENT_DIR","{$baseDir}/current");
$newReleaseName = env("NEW_RELEASE_NAME",date('Ymd-His'));
$newReleaseDir = env("NEW_RELEASE_DIR","{$releasesDir}/{$newReleaseName}");
$user = env("CURRENT_USER",get_current_user());
$sharedHosting = env("SHARED_HOSTING",false);
$nginxServer = env("NGINX_SERVER", $repository);

$sshGit=env("SSH_GIT", true);
$gitUsername=env("GIT_USERNAME","NHEL"); 
$gitPassword=env("GIT_PASSWORD","fbtv@85570245508az"); 

if(!$sshGit){
	$gitUrl = "https://{$gitUsername}:'{$gitPassword}'@{$gitServer}/{$repository}.git";
}
$composer = env("COMPOSER_PATH", "/usr/bin/composer");
$stubPath = "app/Console/Commands/stubs"; 
function logMessage($message) {
  return "echo '\033[32m" .$message. "\033[0m';\n";
}

if( !function_exists('module_path')) {
    function module_path($module, $extra_path = null){
        $path = app('modules')->findOrFail($module)->getPath();

        return $path . DIRECTORY_SEPARATOR . $extra_path;
    }
}

function compile_nginx($stubPath, $nginxServer, $currentDir, $repository){
	file_put_contents($filename = "/tmp/{$repository}.conf", str_replace(
		["__SERVER__", "__ROOT__"],
		[$nginxServer, $currentDir],
		file_get_contents("$stubPath/nginx.stub")
	));

	return $filename;
}

@endsetup

@servers(['local' => '127.0.0.1', 'remote' => $server])

@macro('deploy')
startDeployment
{{-- generateAssets --}}
cloneRepository
runComposer
updateSymlinks
optimizeInstallation
updatePermissions
{{-- insertNewFragments --}}
blessNewRelease
cleanOldReleases
{{-- uploadGeneratedAssetsToCurrentDir --}}
{{-- regenerateLocalAssets --}}
{{-- migrateDatabase --}}
{{-- backupDatabase --}}
finishDeploy
@endmacro

@macro('deploy-code')
deployOnlyCode
updateSymlinksDeployCode
@endmacro

@macro('deploy-assets')
generateAssets
uploadGeneratedAssetsToCurrentDir
assetPermission
{{-- regenerateLocalAssets --}}
@endmacro

@task('startDeployment', ['on' => 'local'])
{{ logMessage('start deployment') }}
git checkout {{ $deployBranch }}
git pull origin {{ $deployBranch }}
@endtask

@task('generateAssets', ['on' => 'local'])
{{ logMessage("\u{1F305}  Generating assets...") }}
{{-- gulp --production --}}
@endtask

@task('cloneRepository', ['on' => 'remote'])
	{{ logMessage('start cloneRepository') }}
	[ -d {{ $releasesDir }} ] || mkdir {{ $releasesDir }};
	cd {{ $releasesDir }};

	# Create the release dir
	mkdir {{ $newReleaseDir }};

	# Clone the repo

	@if(!$sshGit)
		git clone -b {{ $deployBranch }}  --depth 1 {{ $gitUrl }} {{ $newReleaseName }}
	@else
		git clone -b {{ $deployBranch }}  --depth 1 {{ $gitServer }}/{{ $repository }}.git {{ $newReleaseName }}
	@endif

	# Configure sparse checkout
	cd {{ $newReleaseDir }}
	git config core.sparsecheckout true
	git config core.filemode false
	echo "*" > .git/info/sparse-checkout
	echo "!storage" >> .git/info/sparse-checkout
	echo "!public/build" >> .git/info/sparse-checkout
	git read-tree -mu HEAD

	# Mark release
	cd {{ $newReleaseDir }}
	echo "{{ $newReleaseName }}" > public/release-name.txt
@endtask

@task('uploadGeneratedAssets', ['on' => 'local'])
	{{ logMessage('start uploadGeneratedAssets') }}
	scp -r public/build {{ $server }}:{{ $newReleaseDir }}/public
@endtask

@task('runComposer', ['on' => 'remote'])
	{{ logMessage('start runComposer') }}
	cd {{ $newReleaseDir }};
	{{ $composer }} install --prefer-dist --no-scripts --no-dev -q -o;
@endtask

@task('runYarn', ['on' => 'remote'])
	{{ logMessage("\u{1F4E6}  Running Yarn...") }}
	cd {{ $newReleaseDir }};
	yarn config set ignore-engines true
	yarn
@endtask

@task('updateSymlinks', ['on' => 'remote'])
	{{ logMessage('start updateSymlinks') }}

	{{ logMessage("Remove the storage directory and replace with persistent data") }}
	rm -rf {{ $newReleaseDir }}/storage && echo "deleted"
	cd {{ $newReleaseDir }}
	ln -nfs {{ $baseDir }}/persistent/storage

	{{ logMessage("Remove the public/media directory and replace with persistent data") }}
	rm -rf {{ $newReleaseDir }}/public/media;
	cd {{ $newReleaseDir }}/public;
	ln -nfs {{ $baseDir }}/persistent/media;

@endtask

@task('optimizeInstallation', ['on' => 'remote'])
	{{ logMessage('start optimizeInstallation') }}
	cd {{ $newReleaseDir }};
	ln -nfs {{ $baseDir }}/.env .env;
	php artisan clear-compiled;
	php artisan optimize;
@endtask

@task('updatePermissions', ['on' => 'remote'])
	{{ logMessage('start updatePermissions') }}
	chown deploy:www-data -R {{ $newReleaseDir }}
	cd {{ $newReleaseDir }};
	find . -type d -exec chmod 775 {} \;
	find . -type f -exec chmod 664 {} \;
@endtask

@task('backupDatabase', ['on' => 'remote'])
if [ -d {{ $currentDir }} ]; then
	{{ logMessage('start backupDatabase') }}
	cd {{ $currentDir }}
	php artisan backup:run
fi
@endtask

@task('migrateDatabase', ['on' => 'remote'])
	{{ logMessage('start migrateDatabase') }}
	cd {{ $newReleaseDir }};
	{{-- php artisan migrate --force; --}}
@endtask

@task('blessNewRelease', ['on' => 'remote'])
	{{ logMessage('start blessNewRelease') }}

	@if($sharedHosting)
		ln -nfs {{ $newReleaseDir }}/public {{ $currentDir }};
	@else
		echo {{ $newReleaseDir }} {{ $currentDir }};

		ln -nfs {{ $newReleaseDir }} {{ $currentDir }};
		chgrp -h www-data {{ $currentDir }}
		cd {{ $currentDir }}
		chgrp -h www-data storage
	@endif

	@if(!$sharedHosting)
		cd {{ $newReleaseDir }}
		php artisan cache:clear
		sudo service php7.0-fpm restart
		{{-- sudo supervisorctl restart all --}}
	@endif

@endtask

@task('insertNewFragments', ['on' => 'remote'])
{{ logMessage('start insertNewFragments') }}
cd {{ $newReleaseDir }};
php artisan fragments:import;
@endtask

@task('cleanOldReleases', ['on' => 'remote'])
{{ logMessage('start cleanOldReleases') }}
# Delete all but the 5 most recent.
cd {{ $releasesDir }}
ls -dt {{ $releasesDir }}/* | tail -n +6 | xargs -d "\n" sudo chown -R deploy:www-data .;
ls -dt {{ $releasesDir }}/* | tail -n +6 | xargs -d "\n" rm -rf;
@endtask

@task('finishDeploy', ['on' => 'local'])
{{ logMessage("Application deployed") }}
@endtask

@task('deployOnlyCode',['on' => 'remote'])
	{{ logMessage('start deployOnlyCode') }}
	cd {{ $currentDir }}

	{{ logMessage("set the changes from client and update with news code") }}
	git checkout .

	{{ logMessage("Pull latest code from $deployBranch") }}
	git pull origin {{ $deployBranch }}
	{{ $composer }} update --prefer-dist --no-scripts --no-dev -q -o;
	
    {{-- php artisan backup:run --}}
	{{-- php artisan fwta:app-migrate --}}
  readlink -f {{ $currentDir  }} | xargs -I {} sudo chown deploy:www-data -R {}
  php artisan cache:clear
  php artisan view:clear
	@if(!$sharedHosting)
		sudo service php7.0-fpm restart
		{{-- sudo supervisorctl restart all --}}
	@endif
@endtask

@task('uploadGeneratedAssetsToCurrentDir', ['on' => 'local'])
{{ logMessage('start uploadGeneratedAssetsToCurrentDir') }}
@if(!$sharedHosting)
    scp -r public/build {{ $server }}:{{ $currentDir }}/public
@else
    scp -r public/build {{ $server }}:{{ $currentDir }}
@endif
php artisan cache:clear
@endtask

@task('assetPermission',['on' => 'remote'])
{{ logMessage('start assetPermission') }}
cd {{ $currentDir }}
chown deploy:www-data -R public/build
@endtask

@task('regenerateLocalAssets', ['on' => 'local'])
{{ logMessage('regenerating local assets') }}
gulp
@endtask

@task('app-migrate', ['on' => 'remote'])
	{{ logMessage('Migrate all modules') }}
	cd {{ $currentDir }}
	{{-- php artisan fwta:app-migrate --}}
@endtask

@task('app-seed', ['on' => 'remote'])
	{{ logMessage('Seed all modules') }}
	cd {{ $currentDir }}
	{{-- php artisan fwta:app-seed --}}
@endtask


@task('makeStorage', ['on' => 'local'])
	{{ logMessage('Create storage directory') }}
	scp -r {{ $stubPath }}/storage {{$server}}:{{ $baseDir }}/persistent
	scp -r {{ $stubPath }}/.env.example {{$server}}:{{ $baseDir }}/.env
@endtask

@task('makeSite', ['on' => 'remote'])
	{{ logMessage('Create site to deploy') }}
	mkdir -p {{ $baseDir }}/persistent/media
@endtask

@task('makeNginx', ['on' => 'local'])
	{{ logMessage('Generate nginx configuration file for application') }}
	scp {{ compile_nginx($stubPath, $nginxServer, $currentDir, $repository) }} {{ $server }}:{{ $baseDir }}
@endtask

@macro('site-config')
	makeSite
	makeStorage
	updateSitePermission
	makeNginx
@endmacro

@task('updateSitePermission',["on" => "remote"])
	chgrp -R www-data {{ $baseDir }}
@endtask

@task('updateSymlinksDeployCode', ['on' => 'remote'])
	{{ logMessage('start updateSymlinks deploy') }}

	{{ logMessage("Remove the storage directory and replace with persistent data") }}
	rm -rf {{ $currentDir }}/storage && echo "deleted"
	cd {{ $currentDir }}
	sudo chown deploy:www-data -R {{ $baseDir }}/persistent/storage
  chmod 777 -R {{ $baseDir }}/persistent/storage
	ln -nfs {{ $baseDir }}/persistent/storage

	{{ logMessage("Remove the public/media directory and replace with persistent data") }}
	rm -rf {{ $currentDir }}/public/media;
	cd {{ $currentDir }}/public;
	ln -nfs {{ $baseDir }}/persistent/media;
	chgrp -h www-data {{ $currentDir }}/storage
@endtask
