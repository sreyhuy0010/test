<?php 
namespace FWTA\Core\Console\Commands;

use FWTA\Core\Console\Commands\Command;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

/**
 * Translate the the content of lang() function call.
 *
 * @author SoS
 */
class AppSeederCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'fwta:app-seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed all defined modules';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        collect(config('core.module_seeders'))->each(function($module){
            if($module == 'app') $this->call('db:seed');
            else $this->call('module:seed', compact('module'));
        });
    }
}
