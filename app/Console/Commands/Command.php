<?php 

namespace FWTA\Core\Console\Commands;

use Illuminate\Console\Command as BaseCommand;
use Illuminate\Support\Composer;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Config\Repository as Config;
use Illuminate\Console\Scheduling\Schedule;

class Command extends BaseCommand
{
    protected $files;
    protected $config;
    protected $composer;
    protected $stubs = [];

    /**
     * Create a new controller creator command instance.
     *
     * @param \Illuminate\Filesystem\Filesystem $files
     * @param \Illuminate\Config\Repository     $config
     */
    public function __construct(
        Filesystem $files, 
        Config $config, 
        Composer $composer,
        Schedule $schedule

    ){
        parent::__construct();

        $this->files = $files;
        $this->config = $config;
        $this->composer = $composer;
        $this->schedule = $schedule;
    }
    
    protected function className($arg)
    {
        return ucwords(camel_case($this->argument($arg)));
    }
    
     /**
     * Get stub path.
     *
     * @param string $name
     * @return string
     */
    protected function getStubPath($key)
    {
        return __DIR__.$this->stubs[$key];
    }
    
    /**
     * Get stub content
     * 
     * @param  string $name
     * @return string
     */
    protected function getStub($name){
        return $this->files->get($this->getStubPath($name)) ?: '';
    }
    
    /**
     * Compile the stub
     * 
     * @param  string $stub the stub name
     * @param  array $searches
     * @param  array $replaces
     * @return string
     */
    public function compileStub($stub, $searches, $replaces)
    {
        $searches = array_map(function($search){
            return "{{".$search."}}";
        }, !is_array($searches) ? [$searches] : $searches);

        $replaces = !is_array($replaces) ? [$replaces] : $replaces;
         
        return str_replace($searches, $replaces, $this->getStub($stub));
    }
    
}
