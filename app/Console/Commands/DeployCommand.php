<?php  namespace FWTA\Core\Console\Commands;

use FWTA\Core\Console\Commands\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class DeployCommand extends Command {

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Deploy Laravel application';
	
	protected $process = null;
	protected $signature = 'fwta:deploy {ssh=deploy.fankhmer.com}';	
	
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        echo $this->getEnvoyCommandLine();
		$this->process = new Process($this->getEnvoyCommandLine());
		$this->process->setTimeout(3600);

		$this->process->run(function ($type, $buffer) {
		    if (Process::ERR === $type) {
		        $this->error($buffer);
		    } else {
		        $this->info($buffer);
		    }
		});
			
		if (!$this->process->isSuccessful()) {
		    throw new \RuntimeException($this->process->getErrorOutput());
		}
	}
	
	/**
	 * [getEnvoyCommandLine description]
	 * @return [type] [description]
	 */
	protected  function getEnvoyCommandLine(){
		
		$configs = array_except(config('deploy'), ['envoy_path']);
		$configs['ssh'] = $this->argument('ssh') ?: $configs['ssh'];
		$options = $this->buildOption($configs);
		$envoy = config('deploy.envoy_path');
		return "{$envoy} run deploy {$options}";
	}
	
	/**
	 * Build options 
	 * @param  array $options
	 * @return string
	 */
	protected  function buildOption($options){
		
		$opts = "";
		foreach ($options as $key => $value) {
			$opts .= sprintf('--%s=%s ', $key, $value);
		}
		return $opts;
	}
}
