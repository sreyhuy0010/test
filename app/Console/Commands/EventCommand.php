<?php namespace FWTA\Core\Console\Commands;

use Illuminate\Support\Str;
use Pingpong\Modules\Commands\GeneratorCommand;
use Pingpong\Modules\Traits\ModuleCommandTrait;
use Pingpong\Support\Stub;

class EventCommand extends GeneratorCommand {

	use ModuleCommandTrait;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature =  'module:make-event
                                {event : Event name to generate}
                                {module : module that event is belong}
                                {--broadcast : generate}
                            ';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate new repository for the specified module.';


    /**
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());
        $path   =   $this->option('broadcast')
                    ? __DIR__.'/stubs/broadcast.stub'
                    : __DIR__.'/stubs/event.stub';

        return Stub::createFromPath($path, [
	            'NAME'              => $this->getEventName(),
	            'NAMESPACE'         => $this->getClassNamespace($module),
	            'CLASS'             => $this->getClass(),
	            'LOWER_NAME'        => $module->getLowerName(),
	            'ENTITY_NAMESPACE'	=> $this->laravel['modules']->config('paths.generator.event'),
	            'MODULE'            => $this->getModuleName(),
	            'STUDLY_NAME'       => $module->getStudlyName(),
	            'MODULE_NAMESPACE'  => $this->laravel['modules']->config('namespace'),
        ])->render();
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $path = $this->laravel['modules']->getModulePath($this->getModuleName());

        $eventPath = $this->laravel['modules']->config('paths.generator.event');

        return $path.$eventPath.'/'.$this->getEventName().'.php';
    }

    /**
     * @return mixed|string
     */
    private function getEventName()
    {   
        return Str::studly($this->argument('event'));
    }

    /**
     * Get default namespace.
     *
     * @return string
     */
    public function getDefaultNamespace()
    {
        return $this->laravel['modules']->config('paths.generator.event');
    }
}
