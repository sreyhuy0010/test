<?php   namespace FWTA\Core\Console\Commands;

use FWTA\Core\Events\ModelWasGenerated;
use Illuminate\Support\Str;
use Pingpong\Modules\Commands\ModelCommand as ModelBaseCommand;
use Pingpong\Support\Stub;

class ModelCommand extends ModelBaseCommand
{
	/**
     * @return mixed
     */
    protected function getTemplateContents()
    {
        $module = $this->laravel['modules']->findOrFail($this->getModuleName());
        $path   = __DIR__.'/stubs/model.stub';
        
        event(new ModelWasGenerated(
            $this->argument('model'), 
            $module
        ));

        return Stub::createFromPath($path, [
	            'NAME'              => $this->getModelName(),
	            'FILLABLE'          => $this->getFillable(),
	            'NAMESPACE'         => $this->getClassNamespace($module),
	            'CLASS'             => $this->getClass(),
	            'LOWER_NAME'        => $module->getLowerName(),
	            'MODULE'            => $this->getModuleName(),
	            'STUDLY_NAME'       => $module->getStudlyName(),
	            'MODULE_NAMESPACE'  => $this->laravel['modules']->config('namespace'),
        ])->render();
    }

    private function getModelName()
    {
        return Str::studly($this->argument('model'));
    }

    /**
     * @return string
     */
    private function getFillable()
    {
        $fillable = $this->option('fillable');

        if (!is_null($fillable)) {
            $arrays = explode(',', $fillable);

            return json_encode($arrays);
        }

        return '[]';
    }	
}