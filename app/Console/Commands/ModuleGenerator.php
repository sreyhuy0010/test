<?php namespace FWTA\Core\Console\Commands;

use FWTA\Core\Support\Json;
use FWTA\Core\Support\Platform;
use Pingpong\Modules\Generators\ModuleGenerator as BaseModuleGenerator;

class ModuleGenerator extends BaseModuleGenerator {

    public function generate()
    {
        $name = $this->getName();

        if ($this->module->has($name)) {
            if ($this->force) {
                $this->module->delete($name);
            } else {
                $this->console->error("Module [{$name}] already exist!");

                return;
            }
        }

        $this->generateFolders();

        $this->generateFiles();

        if (!$this->plain) {
            $this->generateResources();
        }
        $this->addModuleToComposer()->composerUpdate();
        $this->console->info("Module [{$name}] created successfully.");
    }

    /**
     * Generate symlink for module installing
     * @return [type] [description]
     */
    public function generateSymlink(){

        $path = $this->module->getModulePath($this->getName());
        $vendor_name = $this->module->config('composer.vendor');
        $vendor = base_path()."/vendor/{$vendor_name}/".strtolower($this->getName());
        $this->relativeSymlink($path, $vendor);
        $this->console->info("Symlink created successfully.");
        
    }

    public function addModuleToComposer(){
        $vendor_name = $this->module->config('composer.vendor');
        $package = "{$vendor_name}/".strtolower($this->getName());

        $requires = $this->getComposer()->get('require') + [$package => "@dev"];
        $this->getComposer()->update(['require' => $requires]);

        return $this;
    }

    public function composerUpdate(){
        
        chdir(base_path());
        passthru('composer update');
    }

    /**
     * Create relative symlink
     * @param  string $target
     * @param  string $link
     * @return boolean
     */
    protected function relativeSymlink($target, $link)
    {
        $cwd = getcwd();
        $this->console->info(print_r([$target, $link], true));
        $relativePath = $this->findShortestPath($link, $target);
        chdir(dirname($link));
        $result = @symlink($relativePath, $link);

        chdir($cwd);

        return (bool) $result;
    }

    public function findShortestPath($from, $to, $directories = false)
    {
        if (!$this->isAbsolutePath($from) || !$this->isAbsolutePath($to)) {
            throw new \InvalidArgumentException(sprintf('$from (%s) and $to (%s) must be absolute paths.', $from, $to));
        }

        $from = lcfirst($this->normalizePath($from));
        $to = lcfirst($this->normalizePath($to));

        if ($directories) {
        $from = rtrim($from, '/') . '/dummy_file';
        }

        if (dirname($from) === dirname($to)) {
            return './'.basename($to);
        }

        $commonPath = $to;
        while (strpos($from.'/', $commonPath.'/') !== 0 && '/' !== $commonPath && !preg_match('{^[a-z]:/?$}i', $commonPath)) {
            $commonPath = strtr(dirname($commonPath), '\\', '/');
        }

        if (0 !== strpos($from, $commonPath) || '/' === $commonPath) {
            return $to;
        }

        $commonPath = rtrim($commonPath, '/') . '/';
        $sourcePathDepth = substr_count(substr($from, strlen($commonPath)), '/');
        $commonPathCode = str_repeat('../', $sourcePathDepth);

        return ($commonPathCode . substr($to, strlen($commonPath))) ?: './';
    }

    public function isAbsolutePath($path)
    {
        return substr($path, 0, 1) === '/' || substr($path, 1, 1) === ':';
    }

    protected function normalizePath($path)
    {
        if (Platform::isWindows() && strlen($path) > 0) {
            $basePath = $path;
            $removed = array();

            while (!is_dir($basePath) && $basePath !== '\\') {
                array_unshift($removed, basename($basePath));
                $basePath = dirname($basePath);
            }

            if ($basePath === '\\') {
                return $path;
            }

            $path = rtrim(realpath($basePath) . '/' . implode('/', $removed), '/');
        }

        return $path;
    }

    public function getComposer(){
        return Json::make(base_path('composer.json'));
    }

}
