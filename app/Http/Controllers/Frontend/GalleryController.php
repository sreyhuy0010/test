<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Theavuth\Gallery;

class GalleryController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('current_page', 'galleries');
    }

    public function index()
    {
        $galleries = Gallery::orderBy('id', 'ASC')
                               ->get();

        return view('pages.galleries', compact('galleries'));
    }
    
}
