<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Theavuth\Client;
use Theavuth\Expertise;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Post;

class PostController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('current_page', 'news-and-events');
    }

    public function index()
    {
        $categories = Category::orderBy('id','DESC')->take(6)->get();
        return view('pages.news-and-events2', compact('categories'));
    }

    public function detail($slug)
    {
        $post = Post::where('slug', $slug)
                    ->first();
        return view('pages.news-detail2', compact('post'));
    }

}
