<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Theavuth\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function store(Request $request)
    {

        $data = array_except($request->all(), '_token');
        $this->subscriber->create($data);
        return redirect(route('frontend.index'))->with('status', 'The email was successfully subscribed.');
        
    }

}
