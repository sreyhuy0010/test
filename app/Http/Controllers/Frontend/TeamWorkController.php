<?php

namespace Theavuth\Http\Controllers\Frontend;

use Theavuth\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Theavuth\Client;
use Theavuth\Expertise;
use Theavuth\Team;

class TeamWorkController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        $clients = Client::orderBy('id', 'DESC')
                         ->get();
        $team = Team::orderBy('id','DESC')
                         ->get();

        return view('pages.home', compact('clients', 'team'));
    }

    public function detail($id, $title)
    {
        
    }

}
