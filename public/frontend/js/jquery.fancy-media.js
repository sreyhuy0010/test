jQuery(document).ready(function() {
			
			
	/*
	 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
	*/
	jQuery('.fancybox-media')
		.attr('rel', 'media-gallery')
		.fancybox({
			openEffect : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',

			arrows : false,
			helpers : {
				media : {},
				buttons : {}
			}
		});

	/*
	 *  Open manually
	 */

	jQuery("#fancybox-manual-a").click(function() {
		jQuery.fancybox.open('1_b.jpg');
	});

	jQuery("#fancybox-manual-b").click(function() {
		jQuery.fancybox.open({
			href : 'iframe.html',
			type : 'iframe',
			padding : 5
		});
	});

	jQuery("#fancybox-manual-c").click(function() {
		jQuery.fancybox.open([
			{
				href : '1_b.jpg',
				title : 'My title'
			}, {
				href : '2_b.jpg',
				title : '2nd title'
			}, {
				href : '3_b.jpg'
			}
		], {
			helpers : {
				thumbs : {
					width: 75,
					height: 50
				}
			}
		});
	});


});
