

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
    <!--<![endif]-->
    <head><meta charset="utf-8" />
    <title>
       Welcome to CAMMA Services
    </title>
    <meta name="keywords" content="negotiation skills, presentation skills, negotiation training, time management courses, leadership training, Customer service training, customer service skills, communication training, sales course, time management training, professional development training" /><meta name="description" content="Professional Development Training in presentation skills, leadership training, time management and more. Singapore wide including Singapore and regional centres." /><meta name="msvalidate.01" content="BE14E05B0B7C4997B9D3A696C2119395" />
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><![endif]-->
    <meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
    <link rel="shortcut icon" href="{{ url('assets/images/favicon.ico') }}" />
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/style.css?v=15') }}"/>
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/responsive-nav.css?v=14') }}"/>
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/jquery-ui-1.10.3.custom.min.css') }}"/>
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/fullwidth-style.css') }}"/>
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/new-logo-design.css?v=2') }}"/>
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/font-awesome.min.css') }}"/>
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/fullwidth-menu-top.css') }}"/>
    <link type='text/css' rel='stylesheet' href="{{ url('frontend/css/fullwidth-menu-bottom.css') }}"/>
    <script type='text/javascript' src="{{ url('frontend/js/modernizr.foundation.js') }}"></script>
    <script type='text/javascript' src="{{ url('frontend/js/jquery-1.8.2.min.js') }}"></script>
    <script type='text/javascript' src="{{ url('frontend/js/foundation.js') }}"></script>
    <script type='text/javascript' src="{{ url('frontend/js/turn.min.js') }}"></script>
    <script type='text/javascript' src="{{ url('frontend/js/jquery.jscroll.min.js') }}"></script>
    <script type='text/javascript' src="{{ url('frontend/js/app.js?v=17') }}"></script>
    <script type='text/javascript' src="{{ url('frontend/js/jquery-ui-1.10.3.custom.min.js') }}"></script>


        <!--[if lt IE 9]>
            <script type="text/javascript" src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/google.fonts.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/google.fonts.v2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/jquery.fancybox.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/jquery.fancybox-thumbs.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/pdtraining-homepage-fullwidth.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/owl.carousel.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/owl.theme.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/lightbox-homepage.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ url('frontend/css/owl-demo.css')}}" />

    <script type="text/javascript" src="{{ url('frontend/js/jquery.mousewheel.min.js')}}"></script>
    <script type="text/javascript" src="{{ url('frontend/js/jquery.fancybox.js')}}"></script>	
    <script type="text/javascript" src="{{ url('frontend/js/jquery.fancybox-buttons.js')}}"></script>	
    <script type="text/javascript" src="{{ url('frontend/js/jquery.fancybox-thumbs.js')}}"></script>
    <script type="text/javascript" src="{{ url('frontend/js/jquery.fancybox-media.js')}}"></script>  

    <script type="text/javascript" src="{{ url('frontend/js/lightbox-homepage.js')}}"></script>
    <script type="text/javascript" src="{{ url('frontend/js/owl.carousel.js')}}"></script>
    <script type="text/javascript" src="{{ url('frontend/js/jquery.owl-demo.js')}}"></script>
    <script type="text/javascript" src="{{ url('frontend/js/jquery.fancy-media.js')}}"></script>
    <script type="text/javascript" src="{{ url('frontend/js/custom.js')}}"></script>
    <link type="text/css" rel="stylesheet" href="{{ url('frontend/css/top-banner-rspv2-fullwidth-overwrite.css')}}" />
        
        <style type="text/css">
            /* left */
            #cta .green {
                background-color: #a8cf40 !important;
            }
            #cta .btn-left-content a:hover{
                color: #000;
            }
            #cta .booking-option ul {
                margin: 0px;
            }
            /* middle */
            #cta h1, #cta h2, #cta h3, #cta h4 {
                font-weight: normal;
                line-height: normal;
            }
            #cta h2 {
                margin-bottom: 0px;
                font-size: 1.5em;
            }
            /* right */
            #cta h3 {
                margin-bottom: 0px;
            }
            #cta h2, #cta h3 {
                margin-top: 0px;
            }
            #cta .social-right p {
                font-weight: normal;
                line-height: normal;
            }
            #cta .social-right h3 {
                font-weight: bold;
            }
            #cta .social-right h4 {
                font-size: 15px;
            }
            /* footer bottom */
            .footer_bottom {
                font-size: 12px;
            }
        </style>
        <style type="text/css">
            #aspnetForm {
                margin: 0 0 18px;
            }
        </style>
        

    <style type="text/css">
        .ui-menu {
            z-index: 10001; /* For the search dropdown list */
        }

        nav.menu-new li a
        {
            padding: 13px 35px !important;
        }
    </style>

    <script>
        (function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5423767"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");
    </script>
    </head>

    <body id="ctl00_bodyTag">
        <!-- container -->
        @if (session('status'))
            <div class="alert alert-success div-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="container">            
    <header role="main-header">
        <figure id="logo" class="top-each-section-wrapper">
            <p class="logo-wrapper">
                <a href="{{ url('/') }}" title="CAMMA Services">
                    <img src="{{ url('frontend/images/camma-logo-simple.png')}}" alt="CAMMA Services Logo" class="top-logo" />
                </a>
            </p>
            <div id="ctl00_TopBanner1_divTopMessage" class="top-message-wrapper">
                &nbsp;
            </div>
        </figure>

        <div id="ctl00_TopBanner1_divPhoneDefault" class="search-content">
            <div id="ctl00_TopBanner1_search" class="search">
                <form method="get" id="searchform" action="/webpages/search.aspx">
                    <input type="text" id="q" class="search-box" name="q" placeholder="Search" />
                    <input type="submit" class="submit" name="searchsubmit" id="searchsubmit" value="Search" />
                </form>
            </div>
            <div class="phone-txt">
                <img src="https://pdtraining.com.sg/assets/images/phone.png" />
                <span>
                    <a style='text-decoration: none; color: #5D5D5D;' href='tel:3158 3955'>{{ Voyager::setting('phone_number') }}</a>
                </span>

            </div>
        </div>
        <div class="media">
            <div class="search"></div>
            <div class="social">
                <ul​​ class="icon">
                    <li>
                        <a href="{{Voyager::setting('facebook')}}" target="_blank">
                            <i class="fa fa-facebook-square" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="{{Voyager::setting('linked_in')}}" target="_blank">
                            <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="{{Voyager::setting('youtube')}}" target="_blank">
                            <i class="fa fa-youtube-square" aria-hidden="true" style="color:red"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clr"></div>
    </header>
    </div>
        <!-- container -->        
    <div style="z-index: 500;">
        <div id="divMenuDefault" style="z-index: 510;">
            @include('layouts.nav')
        </div>   
    </div>
    <!-- end nav section -->
    <!-- start Content -->
        @yield('content')
    <!-- end Content -->
    <!--footer //-->
    <div class="bottom">
        <div class="container">
            <div class="bottom-links">            
                <span id="ctl00_MenuBottom2_courseLinksOther">
                    <a href="{{ route('frontend.page', 'what-is-cambodia-management-academy') }}"
                        title="What is Cambodia Management Academy">What is Cambodia Management Academy</a> 
                    <a href="{{ route('frontend.page','our-organizational-chart')}}"
                        title="Our Organizational Chart">Our Organizational Chart</a> 
                    <a href="{{ route('frontend.page','our-vision')}}"
                        title="Our Vission">Our Vision</a> 
                    <a href="{{ route('frontend.page','our-mission')}}"
                        title="Our Mission">Our Mission</a> 
                    <a href="{{ route('frontend.page','our-core-value')}}"
                        title="Career with us">Our Core Value</a>
                    <br>
                    <a href="{{ route('frontend.page','message-from-our-management-team')}}"
                        title="Communication Skills training course">Message from Our Management Team</a> 
                </span>
            </div>
            <div class="copy">
                
                <a href="{{ route('frontend.page','privacy-policy')}}" title="Privacy Policy">Privacy Policy</a> 
                -
                <a href="{{ route('frontend.page','termsandconditions')}}" title="Terms &amp; Conditions">Terms &amp; Conditions</a>
                <br />
                &copy;
                <span id="ctl00_MenuBottom2_copyrightYear">2016 -<?php echo date('Y'); ?></span>
                {{Voyager::setting('government_registered_name')}}
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <!-- Button trigger modal -->
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
        var _vwo_code=(function(){
        var account_id=199367,
        settings_tolerance=2000,
        library_tolerance=2500,
        use_existing_jquery=false,
        // DO NOT EDIT BELOW THIS LINE
        f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->

    <script defer type="text/javascript" src="https://pdtraining.com.sg/assets/scripts/searchterms.js"></script>
    <script defer type="text/javascript" src="https://pdtraining.com.sg/assets/scripts/search.js"></script>
    <script type="text/javascript" src="https://pdtraining.com.sg/assets/css/pdtmobile/js/responsive-nav.js"></script>
    <script>
        var navigation = responsiveNav("#nav", {
            insert: "before"
        });
    </script>
    </body>
</html>

