@extends('layouts.v2')

@section('content')


	@if (!empty($page))
		@if (!empty($page->body))
			<div id="section-landing-wrapper-outer">
		@else
			<div id="section-landing-wrapper-outer" style="margin-bottom: 0px;">
		@endif
			<?php $background = (!empty($page->image)) ? $page->image : ""; ?>
			<div id="section-landing-wrapper" style="background: url({{url('storage/'. $background)}}) center center / cover; display: block; background-color: #0e4f9b;">

			<article id="post-4" class="post-4 page type-page status-publish has-post-thumbnail hentry">
				<header class="entry-header">
					<h1 class="entry-title">{{$page->title}}</h1>	</header><!-- .entry-header -->

				<div class="entry-content">
					<p style="display: block;">{{$page->excerpt}}</p>
						</div><!-- .entry-content -->
				<footer class="entry-footer">
						</footer><!-- .entry-footer -->
			</article><!-- #post-## -->
			</div>
		</div>
		@if (!empty($page->body))
			<div class="clearfixed">&nbsp;</div>
			<div class="page-content" style="mix-height: 330px">

				{!! $page->body !!}
			</div>
		@endif
	@else

		<!-- <div class="secondary-navigation-wrapper">
			<div class="secondary-navigation">
				<div class="menu-level-2-about-us-container">
					<span>About Us</span>
				</div>
			</div>
		</div> -->

		<div class="clearfixed">&nbsp;</div>
		<div class="page-content" style="min-height: 350px !important;">
			<div class="page-content-left">
				<a href="{{ route('frontend.page.detail', 'who-we-are') }}"><h3>Who We Are</h3></a>
			</div>
			<div class="page-content-right">
				<a href="{{ route('frontend.page.detail', 'our-organizational-chart') }}"><h3>Our Organizational Chart</h3></a>
			</div>
		
			<div class="page-content-left">
				<a href="{{ route('frontend.page.detail', 'our-vision') }}"><h3>Our Vision</h3></a>
			</div>
			<div class="page-content-right">
				<a href="{{ route('frontend.page.detail', 'our-mission') }}"><h3>Our Mission</h3></a>
			</div>
		
			<div class="page-content-left">
				<a href="{{ route('frontend.page.detail', 'our-core-value') }}"><h3>Our Core Value</h3></a>
			</div>
			<div class="page-content-right">
				<a href="{{ route('frontend.page.detail', 'message-from-our-management-team') }}"><h3>Message From Our Management Team</h3></a>
			</div>
		</div>
		<div class="clearfixed">&nbsp;</div>
	@endif

@endsection 