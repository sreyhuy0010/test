@extends('layouts.frontend')

@section('content')
	<div class="clearfixed" style="height: 330px"></div>
	<section class="top-heading">
	 	<h2><strong>{{$page->title}}</strong></h2>
	</section>
	<div class="clearfixed">&nbsp;</div>
	<div class="container">
		{!! $page->body !!}


	</div>
	<div class="clearfixed" style="height: 330px"></div>
@endsection