@extends('layouts.v2')

@section('content')

	<section class="top-heading">
	 	<h2 style="text-align:center;"><strong>Gallaries</strong></h2>
	</section>
	<div class="clearfixed">&nbsp;</div>
	<div class="container">
		@if($galleries)
	  		@foreach($galleries as $key => $item)
		    	<div class="col-sm-3" style="border: 1px solid #CCC;">
		    		<a href="#"><img src="{{url('storage/'.$item->image)}}"></a>
		    		<div class="clearfixed"></div>
		    		<div class="row">
			    		<div class="img-gal">{{  $item->name }}</div>
			    	</div>
		    		<div class="clearfixed"></div>
		    	</div>
	    	@endforeach
	    @endif
	</div>
	<div class="clearfixed">&nbsp;</div>
@endsection