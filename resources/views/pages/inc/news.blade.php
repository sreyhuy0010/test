
<div class="subjects-wrapper">
      <h2><strong>Recently News & Events</strong></h2>
      <span class="line-diamond"></span>      
      	@if($posts)
      		<?php $i = 1; ?>
      		@foreach($posts as $key => $item)
	      		@if($i % 4 == 0)
	      			<div class="one-fourth subject-wrap last">
	      		@else
	      			<div class="one-fourth subject-wrap">
	      		@endif
			        <div class="subject-icon">
			        	<a href="{{ route('frontend.post.detail', $item->slug)}}">
			        		<img src="{{ url('storage/'.$item->image) }}" alt="{{ $item->title }}" title="{{ $item->title }}"/>
			        	</a>
			        </div>
			        <div class="subject-caption">
			          	<h3>{{$item->title}}</h3>
			          	<p>{{$item->excerpt}}</p>
			        </div>
			    </div>
		    <?php $i++?>
    		@endforeach
  		@endif
      <div class="clear"></div>
    </div>
</div>