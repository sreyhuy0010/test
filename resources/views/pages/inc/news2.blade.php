
<div class="page-content clear">
<div id="home-news-wrapper" class="clear">
	<div class="index-content row">
    
    	@if($posts)
		<?php $i = 1; ?>
			@foreach($posts as $key => $item)
            <a href="{{ route('frontend.post.detail', $item->slug)}}">
                <div class="col-lg-4">
                    <div class="card">
                        <img src="{{ url('storage/'.$item->image) }}" alt="{{ $item->title }}" title="{{ $item->title }}">
                        <h4>{{$item->created_at}}</h4>
                        <p>{{$item->excerpt}}</p>
                        <a href="{{ route('frontend.post.detail', $item->slug)}}" class="blue-button">Read More</a>
                    </div>
                </div>
            </a>
    		<?php $i++; ?>
			@endforeach
		@endif
    </div>
</div>

</div>                    
<div class="clearfixed">&nbsp;</div>