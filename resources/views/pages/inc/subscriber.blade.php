<div class="container">
	<h2>Signup to keep in touch with all our latest news and events</h2>
	<section  class="search-section" style="background: #2e2e30; padding: 10px 70px;">
	    <div class="row leftmarginlarge">
	        <form method="POST" action="{{ route('subscriber.save') }}">
	        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	            <div class="nine columns">
	                <input type="text" id="txtHomepageSearch" class="search-input" name="q" placeholder="Please input your email"/>
	            </div>
	            <div class="three columns columns_nomargin">
	                <button id="btnHomepageSearch" class="searchBtn" type="submit" title="Subscribe for latest news and events">
	                    &nbsp;
	                    <span>Subscribe</span>
	                </button>
	            </div>
	        </form>
	    </div>
	</section>
	<div class="clear"></div>

</div>