<div class="featured-section">
	<div class="featured-item">
		@if($team)
  			@foreach($team as $key => $item)
			<a href="{{ route('frontend.team.detail', [$item->id, strtolower(str_replace(' ', '-', $item->full_name))]) }}"  class="fancybox-media"  >
			  <img src="{{url('storage/'.$item->avatar)}}" alt="{{$item->full_name}}">
			  <div class="overlay"></div>
			  
			  <span>
				  <h4>{{$item->full_name}}</h4>
				  <h4>{{$item->position}}</h4>
			  </span> 
			</a> 
			@endforeach
  		@endif
	</div>
</div>
