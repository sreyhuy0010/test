<div class="ezcol ezcol-one-half">
	@if($who)
		<h3 id="featured-projects-header">{{$who ->title}}</h3>
		<p>{{$who ->excerpt}}</p>
	@endif
</div>
	
<div class="ezcol ezcol-one-half ezcol-last">
	@if($what)
		<h3 id="featured-projects-header">{{$what->title}}</h3>
		<p>{{$what ->excerpt}}</p>
	@endif
</div>
<div class="ezcol-divider"></div>