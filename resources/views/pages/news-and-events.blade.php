@extends('layouts.frontend')

@section('content')

<div class="clearfixed" style="height: 64px;"></div>
	<section class="top-heading">
	 	<h2><strong>News And Events</strong></h2>
	</section>
	<div class="clearfixed">&nbsp;</div>
	<section>
		<div class="container">
			@if($categories)
				@foreach($categories as $key => $item)
					<div class="col-sm-6">
						<h3>{{ $item->name }}</h3>
						<hr>
						@if($item->posts)
							@foreach($item->posts as $key1 => $post)
								<div class="col-sm-4">
									<a href="{{route('frontend.post.detail', $post->slug)}}">
										<img src="{{ url('storage/'.$post->image) }}">
									</a>
								</div>
								<div class="col-sm-8">
									<a href="{{route('frontend.post.detail', $post->slug)}}">{{ $post->title }}</a>
									<br>
									<p>
										{{ $post->excerpt }}
									</p>
								</div>
							@endforeach
						@endif
					</div>
				@endforeach
			@endif
		</div>
	</section>
	<div class="clearfixed" style="height: 330px"></div>

@endsection