@extends('layouts.v2')

@section('content')
	<div class="clearfixed">&nbsp;</div>
	
	@if($categories)
		@foreach($categories as $key => $item)	

	 <h3 id="featured-projects-header">{{ $item->name }}</h3>
	<div class="page-content " style="mix-height: 330px">
		<!-- <div id="home-news-wrapper" class="clear">
				@if($item->posts)
					@foreach($item->posts as $key => $post)
						<div class="inner-div">
							<div class="home-news-item" id="home-news-item">
								<a href="{{ route('frontend.post.detail', $post->slug)}}">
								</a>
								<img src="{{ url('storage/'.$post->image) }}" alt="{{ $post->title }}" title="{{ $post->title }}"/>
								<h6>{{$post->created_at}}</h6>
								<h4>{{$post->excerpt}}</h4>
							</div>
						</div>
						
					@endforeach
				@endif

		</div>    -->
		<div id="home-news-wrapper" class="clear">
			<div class="index-content row">
		    
		    	@if($item->posts)
					@foreach($item->posts as $key => $post)
		            <a href="{{ route('frontend.post.detail', $post->slug)}}">
		                <div class="col-lg-4">
		                    <div class="card">
		                        <img src="{{ url('storage/'.$post->image) }}" alt="{{ $post->title }}" title="{{ $post->title }}">
		                        <h4>{{$post->created_at}}</h4>
		                        <p>{{$post->excerpt}}</p>
		                        <a href="{{ route('frontend.post.detail', $post->slug)}}" class="blue-button">Read More</a>
		                    </div>
		                </div>
		            </a>
					@endforeach
				@endif
		    </div>
		</div>

	</div>

	@endforeach
	@endif	
@endsection