@extends('layouts.frontend')

@section('content')
	<div class="clearfixed" style="height: 64px;"></div>
	<div class="page-content">
		<section class="top-heading">
		 	<h2><strong>{{$post->title}}</strong></h2>
		</section>
	</div>
	<div class="clearfixed">&nbsp;</div>
	<section>
		<div class="page-content">
			<img src="{{ url('storage/'.$post->image) }}" alt="{{ $post->title }}" title="{{ $post->title }}"/>
			{!! $post->body !!}
		</div>
	</section>
@endsection 